import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodoScreenComponent } from './todo-screen.component';


const appRoutes: Routes = [
    { path: '', component: TodoScreenComponent},
];

export const routing: ModuleWithProviders = RouterModule.forChild(appRoutes);
