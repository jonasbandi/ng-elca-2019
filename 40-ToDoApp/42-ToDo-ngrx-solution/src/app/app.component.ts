import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

@Component({
  selector: 'td-root',
  template: `
      <section class="todoapp">
          <header class="header">
              <h1>Simplistic ToDo</h1>
              <h4>A most simplistic ToDo List with Angular.</h4>
          </header>

          <h4> {{message$ | async}} </h4>
          <router-outlet></router-outlet>

      </section>
      <footer class="info">
          <p>JavaScript Example / Initial template from <a href="https://github.com/tastejs/todomvc-app-template">todomvc-app-template</a>
          </p>
      </footer>
  `,
})
export class AppComponent {
  message$: Observable<string>;

  constructor(private store: Store<{ message: {message: string} }>) {
    this.message$ = this.store.select('message', 'message');
  }
}
