import {showMessage} from './message.actions';
import { completeSuccess, createSuccess, loadSuccess, removeSuccess } from '../todos/todo.actions';
import { Action, createReducer, on } from '@ngrx/store';

export interface IMessageState {
  message: string;
}

const initialState: IMessageState = {message: ''};

const reducer = createReducer(
  initialState,
  on(showMessage, (state, {message}) => ({...state, message})),
  on(completeSuccess, createSuccess, loadSuccess, removeSuccess, (state) => ({...state, message: ''}))
);

export function messageReducer(state: IMessageState | undefined, action: Action) {
  return reducer(state, action);
}
