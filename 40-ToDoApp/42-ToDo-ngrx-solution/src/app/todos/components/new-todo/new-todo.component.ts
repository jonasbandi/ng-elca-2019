import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';
import {ToDo} from '../../model/todos/todo.model';

@Component({
  selector: 'td-new-todo',
  template: `
      <div class="new-todo">
          <form #formRef="ngForm" (ngSubmit)="formRef.valid && onAddToDo()">
              <input #inputRef="ngModel"
                     name="newToDoTitle"
                     [(ngModel)]="newToDoTitle"
                     required minlength="3"
                     type="text"
                     placeholder="What needs to be done???"
                     autofocus autocomplete="off" />
              <button *ngIf="formRef.valid" type="submit" class="add-button" id="add-button" >
                  +
              </button>
          </form>
      </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class NewTodoComponent {

  newToDoTitle = '';
  @Output() addToDo = new EventEmitter<ToDo>();

  onAddToDo(): void {
    this.addToDo.emit(new ToDo(this.newToDoTitle));
    this.newToDoTitle = '';
  }

}
